# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc

tg =selected("TextGrid")
tg_name$ = selected$("TextGrid")

@config.init: "../.log.txt"

beginPause: "Open Sound..."
  sentence: "Sound folder", config.init.return$["sound_folder"]
  word: "Extension", config.init.return$["sound_extension"]
  boolean: "Long sound", 0
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "sound_folder", sound_folder$
@config.setField: "sound_extension", extension$

if long_sound
  sd = Open long sound file: sound_folder$ + "/" + tg_name$ + extension$
else
  sd = Read from file: sound_folder$ + "/" + tg_name$ + extension$
endif

selectObject: sd
plusObject: tg