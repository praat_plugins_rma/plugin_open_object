# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc

sd = selected()
fullName$ =selected$()
type$ = extractWord$(fullName$, "")
if type$ == "Sound" or type$ == "LongSound"
  sd_name$ = selected$(type$)
endif

@config.init: "../.log.txt"

beginPause: "Open TextGrid..."
  sentence: "TextGrid folder", config.init.return$["textgrid_folder"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "textgrid_folder", textGrid_folder$

tg = Read from file: textGrid_folder$ + "/" + sd_name$ + ".TextGrid"

selectObject: tg
plusObject: sd
